<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function submit(Request $request){
        $name = $request['nama'];
        $lastname = $request['last'];
        return view('selamatdatang', compact('name', 'lastname'));
    }
}
